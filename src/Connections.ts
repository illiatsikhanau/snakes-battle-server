import {WebSocket} from 'ws';
import {IUser} from './models/User.js';
import {getUserFromTokenUrl} from './routes/UsersRouter.js';
import {Game} from './Game.js';

class Connection {
  constructor(
    private readonly _user: IUser,
    private readonly _socket: WebSocket,
    private _lastAction: string = '',
    private _game: Game | null = null
  ) {
    this._socket.onmessage = ({data}) => {
      const message = JSON.parse(data.toString());
      switch (message.action) {
        case 'single-player-start':
          this._game = new Game(this.username, this._socket);
          break;
        case 'matching-start':
          Connections.findMatching(this);
          break;
        case 'game-over':
          if (this._game) this._game.gameOver = true;
          break;
        case 'input':
          if (this._game) this._game.handleInput(this._socket, message);
          break;
      }
      this._lastAction = message.action;
    }
  }

  public get username() {
    return this._user.username;
  }

  public get snakeColor() {
    return this._user.snake_color;
  }

  public get lastAction() {
    return this._lastAction;
  }

  public get socket() {
    return this._socket;
  }

  public set lastAction(lastAction: string) {
    this._lastAction = lastAction;
  }

  public set game(game: Game) {
    this._game = game;
  }

  public sendMessage(object: any) {
    this._socket.send(JSON.stringify(object));
  }
}


class Connections {
  private static readonly _connections: Map<string, Connection> = new Map<string, Connection>();
  public static async add(client: WebSocket, url: string | undefined) {
    const user = await getUserFromTokenUrl(url);
    if (user) {
      this._connections.set(user.username, new Connection(user, client));
    } else {
      client.close();
    }
  }

  public static findMatching(client: Connection) {
    const connection = Array.from(this._connections).find(c => c[1].lastAction === 'matching-start');
    if (connection) {
      const client2 = connection[1];
      client2.sendMessage({
        action: 'multi-player-start',
        enemyUsername: client.username,
        enemySnakeColor: client.snakeColor
      });
      client.sendMessage({
        action: 'multi-player-start',
        enemyUsername: client2.username,
        enemySnakeColor: client2.snakeColor,
      });
      const game = new Game(client.username, client.socket, client2.username, client2.socket);
      client.game = game;
      client2.game = game;
      client.lastAction = '';
      client2.lastAction = '';
    }
  }
}

export {Connections};
