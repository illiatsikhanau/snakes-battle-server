import {fileURLToPath} from 'url';
import {dirname} from 'path';
import dotenv from 'dotenv';
import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import {ApiRouter} from './routes/ApiRouter.js';
import {WebSocketServer} from 'ws';
import {Connections} from './Connections.js';

dotenv.config();
const SERVER_HOST = process.env.SERVER_HOST;
const SERVER_PORT = process.env.SERVER_PORT;
const WS_PORT = parseInt(process.env.WS_PORT ?? '');
const DB_USER = process.env.DB_USER;
const DB_PASS = process.env.DB_PASS;
const DB_NAME = process.env.DB_NAME;
const DB_HOST = process.env.DB_HOST;
const DB_PORT = process.env.DB_PORT;

mongoose.connect(`mongodb://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}`).then(() => {
  const app = express();
  const __dirname = dirname(fileURLToPath(import.meta.url));

  app.use(cors({origin: SERVER_HOST, credentials: true}));
  app.use(express.json());
  app.use('/api', ApiRouter);
  app.get('*', express.static(__dirname + '/public'));
  app.get('*', (req, res) => res.sendFile(__dirname + '/public/index.html'));

  app.listen(SERVER_PORT, () => {
    new WebSocketServer({port: WS_PORT}).on('connection', (client, req) => {
      Connections.add(client, req.url);
    });

    console.log(`Server is running at http://localhost:${SERVER_PORT}`)
  });
}).catch(e => console.log(`MongoDB connection error: ${e}`));
