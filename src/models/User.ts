import {Schema, model} from 'mongoose';

interface IUser {
  username: string,
  password: string,
  theme: string,
  snake_color: string,
}

const User = model('User', new Schema<IUser>({
  username: {type: String, required: true, minlength: 3, maxlength: 32},
  password: {type: String, required: true, minlength: 60, maxlength: 60, select: false},
  theme: {type: String, enum: ['light', 'dark']},
  snake_color: {type: String},
}, {versionKey: false}));

export {IUser, User};
