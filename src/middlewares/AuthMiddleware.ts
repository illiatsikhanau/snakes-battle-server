import express from 'express';
import {getUserByAuthorization} from '../routes/UsersRouter.js';

const router = express.Router();

router.use(async (req, res, next) => {
  try {
    if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
      res.sendStatus(401);
    } else {
      const user = await getUserByAuthorization(req.headers.authorization);
      if (user) {
        next();
      } else {
        res.sendStatus(401);
      }
    }
  } catch {
    res.sendStatus(401);
  }
});

export {router as AuthMiddleware};
