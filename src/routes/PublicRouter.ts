import express from 'express';
import {isFreeUsername, signUp} from './UsersRouter.js';

const router = express.Router();

router.post('/check_username', async (req, res) => {
  try {
    res.sendStatus(await isFreeUsername(req.body.username) ? 200 : 400);
  } catch {
    res.sendStatus(400);
  }
});

router.post('/signup', async (req, res) => {
  try {
    res.sendStatus(await signUp(req.body.username, req.body.password, req.body.theme) ? 200 : 400);
  } catch {
    res.sendStatus(400);
  }
});

export {router as PublicRouter};
