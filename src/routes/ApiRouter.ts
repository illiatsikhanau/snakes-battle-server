import express from 'express';
import {PublicRouter} from './PublicRouter.js';
import {AuthMiddleware} from '../middlewares/AuthMiddleware.js';
import {MyRouter} from './MyRouter.js';
import {UsersRouter} from './UsersRouter.js';

const router = express.Router();

router.use(PublicRouter);
router.use(AuthMiddleware);
router.use('/my', MyRouter);
router.use('/users', UsersRouter);
router.use((req, res) => res.sendStatus(404));

export {router as ApiRouter};
