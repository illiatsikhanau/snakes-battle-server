import express from 'express';
import {updateUserById, getUserByAuthorization} from './UsersRouter.js';

const router = express.Router();

router.get('/info', async (req, res) => {
  try {
    const user = await getUserByAuthorization(req.headers.authorization);
    if (user) {
      res.send(user);
    } else {
      res.sendStatus(400);
    }
  } catch {
    res.sendStatus(400);
  }
});

router.patch('/update', async (req, res) => {
  try {
    const user = await getUserByAuthorization(req.headers.authorization);
    const id = user ? user._id.toString() : '';
    await updateUserById(id, req.body);
    res.sendStatus(200);
  } catch {
    res.sendStatus(400);
  }
});

export {router as MyRouter};
