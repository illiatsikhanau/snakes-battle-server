import 'dotenv/config';
import express from 'express';
import bcrypt from 'bcryptjs';
import {IUser, User} from '../models/User.js';

const router = express.Router();

router.get('/', async (req, res) => {
  res.json(await User.find());
});

router.get('/:id', async (req, res) => {
  try {
    res.json(await User.findById(req.params.id));
  } catch {
    res.sendStatus(404);
  }
});

router.post('/', async (req, res) => {
  try {
    const user = new User({
      username: req.body.username,
      password: getHash(req.body.password),
    });

    const savedUser = await user.save();
    res.json(await User.findById(savedUser._id)).status(201);
  } catch {
    res.sendStatus(400);
  }
});

router.patch('/:id', async (req, res) => {
  try {
    res.sendStatus(await updateUserById(req.params.id, req.body) ? 200 : 400);
  } catch {
    res.sendStatus(404);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    await User.deleteOne({_id: req.params.id})
    res.sendStatus(200);
  } catch {
    res.sendStatus(404);
  }
});

const getHash = (string: string) => {
  const saltRounds = Number(process.env.BCRYPT_SALT_ROUNDS ?? 12);
  return bcrypt.hashSync(string, bcrypt.genSaltSync(saltRounds))
};

const updateUserById = (id: string, userData: IUser) => {
  if (userData.password) {
    userData.password = getHash(userData.password);
  }
  return User.findByIdAndUpdate(id, userData);
}

const isFreeUsername = async (username: string | undefined) => {
  try {
    return !await User.findOne({username: username});
  } catch {
    return false;
  }
};

const getUserByAuthorization = async (authorization: string | undefined) => {
  const base64Credentials = authorization ? authorization.split(' ')[1] : '';
  const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii');
  const [username, password] = credentials.split(':');
  const user = await User.findOne({username: username}, '+password');
  if (user && bcrypt.compareSync(password, user.password)) {
    return User.findById(user._id);
  } else {
    return null;
  }
};

const getAuthorizationFromUrl = (url: string | undefined) => {
  const urlSplit = url ? url.split('token=') : '';
  const token = urlSplit.length === 2 ? urlSplit[1] : '';
  return token ? `Basic ${token}` : '';
};

const getUserFromTokenUrl = (url: string | undefined) => getUserByAuthorization(getAuthorizationFromUrl(url));

const signUp = async (username: string, password: string, theme: string) => {
  try {
    const user = new User({
      username: username,
      password: getHash(password),
      theme: theme,
      snake_color: `#${Math.floor(Math.random()*16777215).toString(16)}`,
    });
    return await user.save();
  } catch {
    return null;
  }
};

export {
  router as UsersRouter,
  updateUserById,
  isFreeUsername,
  getUserByAuthorization,
  getUserFromTokenUrl,
  signUp
};
