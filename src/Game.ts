import {WebSocket} from 'ws';

interface Pos {x: number,y: number}
enum Direction {UP, RIGHT, DOWN, LEFT}


class Client {
  constructor(
    private _username: string | null,
    private _socket: WebSocket | null,
    private _snake: Pos[],
    private _direction: number,
    private _directionChange: boolean,
    private _addFood: (foodCount: number) => void
  ) {
  };

  public get username() {
    return this._username;
  };

  public get socket() {
    return this._socket;
  };  
  
  public get snake() {
    return this._snake;
  };

  public set directionChange(directionChange: boolean) {
    this._directionChange = directionChange;
  };

  public isActive = () => {
    return !this._socket || this._socket.readyState === WebSocket.OPEN;
  };

  public sendArena(food: Pos[], enemy: Client) {
    if (this._socket) {
      this._socket.send(JSON.stringify({
        action: 'arena-update',
        snake: this._snake,
        food: food,
        enemy: enemy.snake,
      }));
    }
  };

  public sendGameOver = (winner: string | null) => {
    if (this._socket) {
      this._socket.send(JSON.stringify({
        action: 'game-over',
        winner: winner
      }));
    }
  };

  private getNextPos = (arenaSize: number): Pos => {
    let nextPos = this._snake[0];
    let nextX = nextPos.x;
    let nextY = nextPos.y;
    switch (this._direction) {
      case Direction.UP:
        nextY--;
        break;
      case Direction.RIGHT:
        nextX++;
        break;
      case Direction.DOWN:
        nextY++;
        break;
      default:
        nextX--;
        break;
    }
    return {x: (nextX + arenaSize) % arenaSize, y: (nextY + arenaSize) % arenaSize};
  };

  public canMove = (enemySnake: Pos[], arenaSize: number): boolean => {
    if (this._snake.length) {
      const nextPos = this.getNextPos(arenaSize);
      return !this._snake.find(p => p.x === nextPos.x && p.y === nextPos.y) &&
        !enemySnake.find(p => p.x === nextPos.x && p.y === nextPos.y);
    } else {
      return true;
    }
  };

  public move = (enemy: Client, food: Pos[], arenaSize: number) => {
    if (this._snake.length) {
      const nextPos = this.getNextPos(arenaSize);
      this._snake.unshift(nextPos);
      const foodIndex = food.findIndex(f => nextPos.x === f.x && nextPos.y === f.y);
      if (foodIndex >= 0) {
        this._addFood(1);
        food.splice(foodIndex, 1);
      } else {
        this._snake.pop();
      }
    }
  };

  public handleInput = (input: any) => {
    if (this._directionChange) {
      const direction = input.direction;
      if (
        (direction === Direction.UP && this._direction !== Direction.DOWN) ||
        (direction === Direction.DOWN && this._direction !== Direction.UP) ||
        (direction === Direction.RIGHT && this._direction !== Direction.LEFT) ||
        (direction === Direction.LEFT && this._direction !== Direction.RIGHT)
      ) {
        this._direction = direction;
        this._directionChange = false;
      }
    }
  };
}


class Game {
  private readonly _arenaSize: number = 12;
  private readonly _foodCount: number = 2;
  private readonly _updateDelay: number = 70;

  private readonly _client1: Client;
  private readonly _client2: Client;
  private readonly _food: Pos[];
  private _gameOver: boolean = false;

  constructor(username1: string, socket1: WebSocket, username2: string | null = null, socket2: WebSocket | null = null) {
    const client2Snake = socket2 ? [{x: this._arenaSize - 1, y: this._arenaSize - 1}] : [];
    this._client1 = new Client(username1, socket1, [{x: 0, y: 0}], Direction.RIGHT, true, this.addFood);
    this._client2 = new Client(username2, socket2, client2Snake, Direction.LEFT, true, this.addFood);
    this._food = [];
    this.addFood(this._foodCount);
    this.startGame();
  };

  private sleep = (delay: number) => new Promise(resolve => setTimeout(resolve, delay));

  public get gameOver() {
    return this._gameOver;
  }

  public set gameOver(gameOver: boolean) {
    this._gameOver = gameOver;
  }

  private addFood = (foodCount: number) => {
    for (let f = 0; f < foodCount; f++) {
      const indexes = Array(this._arenaSize * this._arenaSize).fill(true);
      this._client1.snake.forEach(p => indexes[p.x + p.y * this._arenaSize] = false);
      this._client2.snake.forEach(p => indexes[p.x + p.y * this._arenaSize] = false);
      this._food.forEach(p => indexes[p.x + p.y * this._arenaSize] = false);
      const freeIndexes = indexes.map((p, index) => p ? index : -1).filter(p => p >= 0);

      if (freeIndexes.length) {
        const freeIndex = freeIndexes[Math.floor(Math.random() * freeIndexes.length)];
        this._food.push({
          x: freeIndex % this._arenaSize,
          y: Math.floor(freeIndex / this._arenaSize)
        });
      }
    }
  };

  private startGame = async () => {
    while (!this.gameOver && this._client1.isActive() && this._client2.isActive()) {
      this._client1.sendArena(this._food, this._client2);
      this._client2.sendArena(this._food, this._client1);
      await this.sleep(this._updateDelay);

      if (this._client1.canMove(this._client2.snake, this._arenaSize)) {
        this._client1.move(this._client2, this._food, this._arenaSize);
        this._client1.directionChange = true;
      } else {
        this.gameOver = true;
        break;
      }

      this._client1.sendArena(this._food, this._client2);
      this._client2.sendArena(this._food, this._client1);
      await this.sleep(this._updateDelay);

      if (this._client2.canMove(this._client1.snake, this._arenaSize)) {
        this._client2.move(this._client1, this._food, this._arenaSize);
        this._client2.directionChange = true;
      } else {
        this.gameOver = true;
        break;
      }
    }

    let winner = this._client1.username;
    if (this._client1.snake.length < this._client2.snake.length) {
      winner = this._client2.username;
    }
    this._client1.sendGameOver(winner);
    this._client2.sendGameOver(winner);
  };

  public handleInput = (socket: WebSocket, input: any) => {
    socket === this._client1.socket && this._client1.handleInput(input);
    socket === this._client2.socket && this._client2.handleInput(input);
  };
}

export {Game};
